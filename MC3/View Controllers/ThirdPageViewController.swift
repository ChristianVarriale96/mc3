//
//  NewViewController.swift
//  PageControl
//
//  Created by Christian Varriale on 18/03/2020.
//  Copyright © 2020 Seemu. All rights reserved.
//

import UIKit

class ThirdPageViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        designButton()
    }
    
    func designButton(){
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.layer.cornerRadius = button.frame.height / 2
        button.setTitleColor(#colorLiteral(red: 0.8588235294, green: 0.4901960784, blue: 0, alpha: 1), for: .normal)
        button.setTitle("Start searching", for: .normal)
        button.layer.shadowColor = UIColor.darkGray.cgColor
        button.layer.shadowRadius = 4
        button.layer.shadowOpacity = 0.5
        button.layer.shadowOffset = CGSize(width: 0, height: 0)
    }

    @IBAction func gotTapped(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        
        UserDefaults.standard.set(true, forKey: "OnBoardingComplete")
        
        userDefaults.synchronize()
        
        let storyboard = UIStoryboard(name: "App", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "App")
        self.present(controller, animated: false, completion: nil)
    }
    
}
