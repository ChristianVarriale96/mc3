//
//  LocationService.swift
//  MC3
//
//  Created by Christian Varriale on 02/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

//MARK: - Protocol
protocol LocationServiceDelegate {
    func locationDidUpdateToLocation(currentLocation: CLLocation)
    func locationUpdateDidFailWithError(error: NSError)
}

///Definizione del Singleton in modo che venga garantita un'unics classe di questa istanza, che permetta un accesso globale. All'interno abbiamo definito la lastLocation impostata a Napoli, il delegate, il location manager ed infine la relativa istanza che definita static sarà visibile durante l'esecuzione del programma all'interno dell'istanza
//MARK: - Class Definition
class LocationSingleton: NSObject,CLLocationManagerDelegate {
    
    //MARK: - Variable
    var lastLocation = CLLocation()
    
    var delegate: LocationServiceDelegate?
    
    static let sharedInstance = LocationSingleton()
    
    let locationManager = CLLocationManager()
    
    //MARK: - Constructor
    override init() {
        super.init()
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.distanceFilter = 0.1
        locationManager.delegate = self
        
    }
    
    //MARK: - Function
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        self.lastLocation = location
        updateLocation(currentLocation: location)
        
    }
    
    @nonobjc func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
            break
        case .restricted:
            print("Unauthorized to use location service")
            break
        case .denied:
            // user denied your app access to Location Services, but can grant access from Settings.app
            break
        default:
            break
        }
    }
    
    //MARK: - Private Function
    private func updateLocation(currentLocation: CLLocation){
        delegate?.locationDidUpdateToLocation(currentLocation: currentLocation)
    }
    
    private func updateLocationDidFailWithError(error: NSError) {
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.locationUpdateDidFailWithError(error: error)
    }
    
    //MARK: - Start Update, Stop Update and Start Monitoring
    func startUpdatingLocation() {
        print("Starting Location Updates")
        self.locationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation() {
        print("Stop Location Updates")
        self.locationManager.stopUpdatingLocation()
    }
    
    func startMonitoringSignificantLocationChanges() {
        self.locationManager.startMonitoringSignificantLocationChanges()
    }
    
}
