//
//  FilterViewController.swift
//  FiltersView
//
//  Created by Stefano Di Nunno on 17/02/2020.
//  Copyright © 2020 Stefano Di Nunno. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

//MARK: - Class Definition

class FilterViewController: UIViewController {
    
    //MARK: - IBOutlet
    @IBOutlet weak var filterMainView: UIView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    
    // Places Outlets
    @IBOutlet weak var HistoricalCenterIcon: UIButton!
    @IBOutlet weak var CenterIcon: UIButton!
    @IBOutlet weak var VomeroIcon: UIButton!
    @IBOutlet weak var ChiaiaIcon: UIButton!
    
    // Categories Outlets
    @IBOutlet weak var FoodIcon: UIButton!
    @IBOutlet weak var DrinkIcon: UIButton!
    @IBOutlet weak var ShopIcon: UIButton!
    
    //MARK: - Outlet Label
    @IBOutlet weak var HistoricalCenterLabel: UILabel!
    @IBOutlet weak var CenterLabel: UILabel!
    @IBOutlet weak var VomeroLabel: UILabel!
    @IBOutlet weak var ChiaiaLabel: UILabel!
    @IBOutlet weak var FoodLabel: UILabel!
    @IBOutlet weak var DrinkLabel: UILabel!
    @IBOutlet weak var ShopLabel: UILabel!
    
    //MARK: View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the vie
        setOldFilters()
        
        confirmButtonDesign()
        filterMainView.layer.cornerRadius = 10
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        filterMainView.showVertically()
    }
    
    //MARK: - Filters Function
    func setOldFilters() {
        if Model.currentModel.getAreaFilter() == "Historical Center" {
            HistoricalCenterIcon.isSelected = true
            CenterIcon.isSelected = false
            VomeroIcon.isSelected = false
            ChiaiaIcon.isSelected = false
        }else if Model.currentModel.getAreaFilter() == "Center" {
            HistoricalCenterIcon.isSelected = false
            CenterIcon.isSelected = true
            VomeroIcon.isSelected = false
            ChiaiaIcon.isSelected = false
        }else if Model.currentModel.getAreaFilter() == "Vomero" {
            HistoricalCenterIcon.isSelected = false
            CenterIcon.isSelected = false
            VomeroIcon.isSelected = true
            ChiaiaIcon.isSelected = false
        }else if Model.currentModel.getAreaFilter() == "Chiaia" {
            HistoricalCenterIcon.isSelected = false
            CenterIcon.isSelected = false
            VomeroIcon.isSelected = false
            ChiaiaIcon.isSelected = true
        }
        
        if Model.currentModel.getCategoryFilter() == "Food" {
            FoodIcon.isSelected = true
            DrinkIcon.isSelected = false
            ShopIcon.isSelected = false
        }else if Model.currentModel.getCategoryFilter() == "Drink" {
            FoodIcon.isSelected = false
            DrinkIcon.isSelected = true
            ShopIcon.isSelected = false
        }else if Model.currentModel.getCategoryFilter() == "Shop" {
            FoodIcon.isSelected = false
            DrinkIcon.isSelected = false
            ShopIcon.isSelected = true
        }
    }
    
    //MARK: - Touch Began -> when the tap is out the filterMainView, it go down
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.view) else { return }
        if !filterMainView.frame.contains(location) {
            filterMainView.showVerticallyDown()
            dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: IBAction
    @IBAction func confirmButtonPressed(_ sender: Any) {
        filterMainView.showVerticallyDown()
        self.performSegue(withIdentifier: "applicaFiltri", sender: self)    //Unwind segue
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        filterMainView.showVerticallyDown()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func historicalFilter(_ sender: UIButton) {
        if sender.isHighlighted{
            Model.currentModel.setAreaFilter(area: "Historical Center")
        }
        
        HistoricalCenterIcon.isSelected = !HistoricalCenterIcon.isSelected
        
        if HistoricalCenterIcon.isSelected{
            HistoricalCenterLabel.textColor = #colorLiteral(red: 0.968627451, green: 0.5764705882, blue: 0.1176470588, alpha: 1)
            CenterLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            VomeroLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            ChiaiaLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }else{
            HistoricalCenterLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }
        
        CenterIcon.isSelected = false
        VomeroIcon.isSelected = false
        ChiaiaIcon.isSelected = false
        
    }
    
    @IBAction func centerFilter(_ sender: UIButton) {
        if sender.isHighlighted{
            Model.currentModel.setAreaFilter(area: "Center")
        }
        CenterIcon.isSelected = !CenterIcon.isSelected
        
        if CenterIcon.isSelected{
            CenterLabel.textColor = #colorLiteral(red: 0.968627451, green: 0.5764705882, blue: 0.1176470588, alpha: 1)
            HistoricalCenterLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            VomeroLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            ChiaiaLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }else{
            CenterLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }
        
        HistoricalCenterIcon.isSelected = false
        VomeroIcon.isSelected = false
        ChiaiaIcon.isSelected = false
        
    }
    
    @IBAction func vomeroFilter(_ sender: UIButton) {
        if sender.isHighlighted{
            Model.currentModel.setAreaFilter(area: "Vomero")
        }
        
        VomeroIcon.isSelected = !VomeroIcon.isSelected
        
        if VomeroIcon.isSelected{
            VomeroLabel.textColor = #colorLiteral(red: 0.968627451, green: 0.5764705882, blue: 0.1176470588, alpha: 1)
            HistoricalCenterLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            CenterLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            ChiaiaLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }else{
            VomeroLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }
        
        CenterIcon.isSelected = false
        HistoricalCenterIcon.isSelected = false
        ChiaiaIcon.isSelected = false
        
    }
    
    @IBAction func chiaiaFilter(_ sender: UIButton) {
        if sender.isHighlighted{
            Model.currentModel.setAreaFilter(area: "Chiaia")
        }
        
        ChiaiaIcon.isSelected = !ChiaiaIcon.isSelected
        
        if ChiaiaIcon.isSelected{
            ChiaiaLabel.textColor = #colorLiteral(red: 0.968627451, green: 0.5764705882, blue: 0.1176470588, alpha: 1)
            HistoricalCenterLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            CenterLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            VomeroLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }else{
            ChiaiaLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }
        
        CenterIcon.isSelected = false
        HistoricalCenterIcon.isSelected = false
        VomeroIcon.isSelected = false
    }
    
    @IBAction func foodFilter(_ sender: UIButton) {
        if sender.isHighlighted{
            Model.currentModel.setCategoryFilter(category: "Food")
        }
        
        FoodIcon.isSelected = !FoodIcon.isSelected
        
        if FoodIcon.isSelected{
            FoodLabel.textColor = #colorLiteral(red: 0.968627451, green: 0.5764705882, blue: 0.1176470588, alpha: 1)
            DrinkLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            ShopLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }else{
            FoodLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }
        
        DrinkIcon.isSelected = false
        ShopIcon.isSelected = false
    }
    
    @IBAction func drinkFilter(_ sender: UIButton) {
        if sender.isHighlighted{
            Model.currentModel.setCategoryFilter(category: "Drink")
        }
        
        DrinkIcon.isSelected = !DrinkIcon.isSelected
        
        if DrinkIcon.isSelected{
            DrinkLabel.textColor = #colorLiteral(red: 0.968627451, green: 0.5764705882, blue: 0.1176470588, alpha: 1)
            FoodLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            ShopLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }else{
            DrinkLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }
        
        FoodIcon.isSelected = false
        ShopIcon.isSelected = false
    }
    
    @IBAction func shopFilter(_ sender: UIButton) {
        if sender.isHighlighted{
            Model.currentModel.setCategoryFilter(category: "Shop")
        }
        
        ShopIcon.isSelected = !ShopIcon.isSelected
        
        if ShopIcon.isSelected{
            ShopLabel.textColor = #colorLiteral(red: 0.968627451, green: 0.5764705882, blue: 0.1176470588, alpha: 1)
            FoodLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            DrinkLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }else{
            ShopLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }
        
        FoodIcon.isSelected = false
        DrinkIcon.isSelected = false
    }
    
    //MARK: Function Confirm
    func confirmButtonDesign() {
        confirmButton.layer.cornerRadius = confirmButton.frame.height/2
    }
}
