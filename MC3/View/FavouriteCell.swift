//
//  RestaurantCell.swift
//  MC3
//
//  Created by Stefano Di Nunno on 18/02/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Class Definition
class FavouriteCell: UITableViewCell {
    
    ///Contenuto della cella, collegato alle relative IBOutlet
    //MARK: - IBOutlet
    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var favouriteCellButton: UIButton!
    
    ///Settaggio della variabile restaurant in base agli outlet superiori. 
    //MARK: - Properties
    var restaurant: Ristorante? {
        didSet {
            name!.text = restaurant?.name
            address!.text = restaurant?.address
            imgView?.image = UIImage(named: restaurant!.cardPhoto)
        }
    }
    
    ///funzione, che prende il parametro ristorante, e verrà usata per settare ogni cella nel richiamo della funzione della tableView
    func configure (restaurant : Ristorante) {
        self.name!.text = restaurant.name
        self.address!.text = restaurant.address
        self.imgView?.image = UIImage(named: restaurant.cardPhoto)
    }
    
    //MARK: - Cell Function
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    ///Con questa funzione: !sender.isSelected -> vuol dire cuore che da pieno diventa vuoto, quindi lo rimuovo dai preferiti, altrimenti lo aggiungo
    /// Se sender.tag.selected = true -> rimuovi da preferiti
    /// Else -> aggiungi
    @IBAction func favouriteCellButtonPressed(_ sender: UIButton) {
        
        if !sender.isSelected {
            // remove
            CoreDataController.sharedIstance.deleteFavouriteRestaurant(idRestaurant: self.name!.text!)
        }else {
            // add
            CoreDataController.sharedIstance.addFavouriteRestaurant(idRestaurant: self.name!.text!)
        }
        sender.isSelected = !sender.isSelected
        
    }
}
